#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:134217728:bab3bd900a187fc2e24cc09e45ed030fd85f353b; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:100663296:79375ff009cec25f99b9b93f7b4bad61a501c6e1 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:134217728:bab3bd900a187fc2e24cc09e45ed030fd85f353b && \
      log -t recovery "Installing new oplus recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oplus recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
